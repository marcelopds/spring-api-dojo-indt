package org.indt.dojo.java.rest.springapidojoindt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringApiDojoIndtApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringApiDojoIndtApplication.class, args);
	}
}
